import React, { useState } from 'react'

import { SliderReverse } from 'meterial-ui-reverse-slider';
import 'meterial-ui-reverse-slider/dist/index.css'

const App = () => {

  let [x, setx] = useState(0);
  let style = {
    container: {
      width: "400px",
      margin: "300px auto"
    },
    header: {
      marginBottom: '30px'
    }
  }

  return (
    <div style={style.container}>
      <h2 style={style.header}>Meterial UI Reverse Slider Example</h2>

      <span>Value : {x}</span>
      <SliderReverse onChange={setx} max={100} value={x} />
    </div>);
}

export default App
