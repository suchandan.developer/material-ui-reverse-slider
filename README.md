# material-ui-reverse-slider

> This is a package to make the Meterial UI slider in reverse order.

[![NPM](https://img.shields.io/npm/v/meterial-ui-reverse-slider.svg)](https://www.npmjs.com/package/meterial-ui-reverse-slider) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save material-ui-reverse-slider
```

## Usage

```jsx
import React, { Component } from 'react'

import { SliderReverse } from 'material-ui-reverse-slider'

class MyComponent extends Component {

  state = {
    x : 0
  };

  setx = (value) => {
    this.setState({ x : value });
  }
  
  render() {
    return <SliderReverse onChange={this.setx} max={100} value={this.state.x} />
  }
}
```
## Dependency 

```
This component is dependent on Material UI package. Please take a look at Slider Component.  

```
## Links
[Material UI Slider](https://material-ui.com/components/slider/)


## Options
```
Options are same as the options provided by the Material UI Slider. 

```

## License

MIT © [mete-ui-reverse-slider](https://gitlab.com/suchandan.developer/material-ui-reverse-slider)
