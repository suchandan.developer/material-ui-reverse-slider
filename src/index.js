import React from 'react';
import { Slider } from '@material-ui/core';


let ReverseSlider = (props) => {
  return (<Slider {...props}
    track={props.track !== undefined ? props.track : 'inverted'}
    value={(props.max - props.value)}
    onChange={(event, nv) => props.onChange((props.max - nv), event)}
    valueLabelFormat={props.valueLabelFormat ? props.valueLabelFormat : (value) => (props.max - value)}
    getAriaValueText={props.getAriaValueText ? props.getAriaValueText : (value) => (props.max - value)} />
  );
}

ReverseSlider.prototype = Slider.prototype;

export const SliderReverse = ReverseSlider;